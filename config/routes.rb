# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :v1 do
    scope 'media' do
      post 'avaliacao', to: '/v1/avaliacao#create'
      get 'avaliacao', to: '/v1/avaliacao#index'

      post 'projetos', to: '/v1/projetos#create'
      get 'projetos', to: '/v1/projetos#index'

      post 'criterios', to: '/v1/criterios#create'
      get 'criterios', to: '/v1/criterios#index'

      post 'notas_avaliacao', to: '/v1/notas_avaliacao#create'
      get 'notas_avaliacao', to: '/v1/notas_avaliacao#index'
    end
  end
end
