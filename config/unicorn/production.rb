# frozen_string_literal: true

# Should be 'production' by default, otherwise use other env
# rails_env = 'production'

# Set unicorn options
worker_processes 3
preload_app true
timeout 50
listen '/tmp/dominios.socket', backlog: 64

# Spawn unicorn master worker for user apps (group: apps)
# user 'dominios'

# Fill path to your app
# working_directory app_path

# Log everything to one file
# stderr_path '/var/log/dominios/error.log'
# stdout_path '/dev/null'

# Set master PID location
# pid '/tmp/dominios.pid'

before_fork do |_server, _worker|
  ActiveRecord::Base.connection.disconnect!
end

after_fork do |_server, _worker|
  ActiveRecord::Base.establish_connection
end
