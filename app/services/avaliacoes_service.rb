# AvaliacoesService
# Classe de servico responsavel por gestionar Avaliacoes, criando ou atualizando.

class AvaliacoesService
  class << self
    # Cria ou atualiza uma avaliacao
    def cria_atualiza(dados)
      avaliacao = avaliacao_instance(dados)
      avaliacao.update!(dados)
      avaliacao
    rescue StandardError => e
      raise "Falha ao gravar avaliacao: #{e.message}"
    end

    # Salva a media ponderada
    def salva_media_ponderada(dados, media)
      avaliacao = avaliacao_instance(dados)
      avaliacao.media_ponderada = media
      avaliacao.save
    end

    private 

    # Retorna a instancia de Avaliacao
    def avaliacao_instance(dados)
      dados[:id] = nil if dados[:id].present? && dados[:id].to_i.zero?
      Avaliacao.where(id: dados[:id]).first_or_initialize
    end
  end
end