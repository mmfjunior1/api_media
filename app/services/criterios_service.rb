# CriteriosService
# Classe de servico responsavel por gestionar Criterios, criando ou atualizando.
class CriteriosService
  class << self
    # Cria ou atualiza um criterio
    def cria_atualiza(dados)
      criterio = criterio_instance(dados)
      criterio.update(dados)
      criterio
    rescue StandardError => e
      raise "Falha ao gravar criterio: #{e.message}"
    end

    private 

    # Retorna a instancia de Criterio
    def criterio_instance(dados)
      dados[:id] = nil if dados[:id].present? && dados[:id].to_i.zero?
      Criterio.where(id: dados[:id]).first_or_initialize
    end
  end
end