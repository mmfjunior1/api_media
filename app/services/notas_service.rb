
# NotaService
# Classe de servico responsavel por gestionar notas, inserindo ou atualizando
class NotasService
  class << self
    # Cria ou atualiza uma nota, calculando a media ponderada e total
    def cria_atualiza(dados)
      nota = nota_instance(dados)
      nota.update!(dados)
      calcular_medias(nota)
      nota
    rescue StandardError => e
      raise "Falha ao gravar nota: #{e.message}"
    end

    private 

    # Retorna a instancia de Nota
    def nota_instance(dados)
      dados[:id] = nil if dados[:id].present? && dados[:id].to_i.zero?
      Nota.where(id: dados[:id]).first_or_initialize
    end

    # Calcula as medias totais e ponderadas
    def calcular_medias(nota)
      media_ponderada = MediaService.calcular_ponderada(nota)
      media_total = MediaService.calcular_total(nota)
      AvaliacoesService.salva_media_ponderada({ id: nota.avaliacao_id }, media_ponderada)
      ProjetosService.salva_media_total(nota, media_total)
    end
  end
end