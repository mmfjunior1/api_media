
# MediaService
# Classe de servico responsavel por calcular medias
class MediaService
  class << self
    # Calcula a media ponderada
    def calcular_ponderada(nota)
      notas = Nota.where(criterio_id: nota.criterio_id)
      dividendo = 0
      divisor = 0
      notas.each do |nota|
        peso = nota.criterio.peso
        dividendo = dividendo + (peso * nota.nota)
        divisor = divisor + peso
      end
      dividendo / divisor
    end

    # Calcula a media total
    def calcular_total(nota)
      projeto_id = nota.avaliacao.projeto.id
      avaliacoes = Avaliacao.where(projeto_id: projeto_id)
      dividendo = 0
      divisor = 0
      avaliacoes.each do |avaliacao|
        dividendo = dividendo + avaliacao.media_ponderada
        divisor = divisor + 1
      end
      dividendo / divisor
    end
  end
end