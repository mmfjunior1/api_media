# ProjetosService
# Classe de servico responsavel por gestionar Projetos, criando ou atualizando.
class ProjetosService
  class << self
    # Cria ou atualiza um projeto
    def cria_atualiza(dados)
      projeto = projeto_instance(dados)
      projeto.update(dados)
      projeto
    rescue StandardError => e
      raise "Falha ao gravar Projeto: #{e.message}"
    end

    # Salva a media total
    def salva_media_total(nota, media_total)
      projeto_id = nota.avaliacao.projeto.id
      Projeto.find(projeto_id).update({media_total: media_total})
    end

    private 

    # Retorna a instancia de Projeto
    def projeto_instance(dados)
      dados[:id] = nil if dados[:id].present? && dados[:id].to_i.zero?

      Projeto.where(id: dados[:id]).first_or_initialize
    end
  end
end