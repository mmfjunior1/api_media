module V1
  class AvaliacaoController < ApplicationController
    def index
      params[:qtde] = 25 if params[:qtde].to_i > 100
      avaliacoes = Avaliacao.paginate(page: params[:page] || 1, per_page: params[:qtde] || 25)
      render json: avaliacoes, status: :ok
    end

    def create
      avaliacao = AvaliacoesService.cria_atualiza(set_params)
      render json: avaliacao, status: :created
    rescue StandardError => e
      render json: { error: e.message }, status: :unprocessable_entity
    end

    private

    def set_params
      params.require(:avaliacao).permit(:projeto_id, :id)
    end
  end
end
