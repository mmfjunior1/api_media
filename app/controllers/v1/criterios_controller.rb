module V1
  class CriteriosController < ApplicationController
    def index
      params[:qtde] = 25 if params[:qtde].to_i > 100
      criterios = Criterio.paginate(page: params[:page] || 1, per_page: params[:qtde] || 25)
      render json: criterios, status: :ok
    end

    def create
      criterio = CriteriosService.cria_atualiza(set_params)
      render json: criterio, status: :created
    rescue StandardError => e
      render json: { error: e.message }, status: :unprocessable_entity
    end

    private

    def set_params
      params.require(:criterio).permit(:peso, :id)
    end
  end
end
