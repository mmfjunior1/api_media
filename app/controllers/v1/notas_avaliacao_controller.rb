module V1
  class NotasAvaliacaoController < ApplicationController
    def index
      params[:qtde] = 25 if params[:qtde].to_i > 100
      notas = Nota.paginate(page: params[:page] || 1, per_page: params[:qtde] || 25)
      render json: notas, status: :ok
    end

    def create
      nota = NotasService.cria_atualiza(set_params)
      render json: nota, status: :created
    rescue StandardError => e
      render json: { error: e.message }, status: :unprocessable_entity
    end

    private

    def set_params
      params.require(:notas_avaliacao).permit(:avaliacao_id, :criterio_id, :nota, :id)
    end
  end
end
