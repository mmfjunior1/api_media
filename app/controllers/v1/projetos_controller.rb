module V1
  class ProjetosController < ApplicationController
    def index
      params[:qtde] = 25 if params[:qtde].to_i > 100
      projetos = Projeto.paginate(page: params[:page] || 1, per_page: params[:qtde] || 25)
      render json: projetos, status: :ok
    end

    def create
      projeto = ProjetosService.cria_atualiza(set_params)
      render json: projeto, status: :created
    rescue StandardError => e
      render json: { error: e.message }, status: :unprocessable_entity
    end

    private

    def set_params
      params.require(:projeto).permit(:nome, :media_total, :id)
    end
  end
end
