class Nota < ApplicationRecord
  self.table_name = 'notas'
  
  belongs_to :criterio
  belongs_to :avaliacao
end
