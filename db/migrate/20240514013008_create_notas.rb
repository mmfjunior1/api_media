class CreateNotas < ActiveRecord::Migration[7.1]
  def change
    create_table :notas do |t|
      t.float :nota
      t.references :criterio, null: false, foreign_key: true
      t.integer :avaliacao_id
      
      t.timestamps
    end
  end
end
