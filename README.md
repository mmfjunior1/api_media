# api_media

Api realiza cálculos de média ponderada e média total.

## Atenção

Se você utiliza Docker em sistemas com processadores arm talvez você tenha problemas para realizar o build.
Eu disse "talvez". Havendo, você deve garantir que seu Docker engine execute o build com --plaform linux/amd64.

## Funcionalidades

 - Cadastro de Projeto
 - Cadastro de Criterios
 - Cadastro de Notas
 - Cadastro de avaliações

## Endpoints

- POST /v1/media/avaliacao
- GET  /v1/media/avaliacao
- POST /v1/media/projetosprojetos#create
- GET  /v1/media/projetos
- POST /v1/media/criterioscriterios#create
- GET  /v1/media/criterios
- POST /v1/media/notas_avaliacaonotas_avaliacao#create
- GET  /v1/media/notas_avaliacao

## Execução do projeto
```
docker compose build
docker compose up app

```

## Acessando o projeto

Escolha o aplicativo de sua preferência e acesse o projeto por http://127.0.0.1:3000

## Execução dos testes
```
docker compose  -f docker-compose-test.yml build
docker compose  -f docker-compose-test.yml run app

```
A primeira execução pode levar, em média, 10 segundos pois o entrypoint aguarda o banco de dados subir.
