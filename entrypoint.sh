#!/bin/bash

# Aguarda o banco de dados
echo "Aguarde"
sleep 10
# Verifica se o banco de dados existe
if ! mysql -u root -pmedia -h db -e 'use media'; then
    echo "Restaurando o banco de dados"

    # Restore
    mysql -u root -pmedia -h db < dump.sql

    echo "Restauração concluída."
fi


# Inicia o serviço
exec "$@"
