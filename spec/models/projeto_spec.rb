require 'rails_helper'

RSpec.describe Projeto, type: :model do

  describe "Valida gravacao na tabela" do
    before do
      FactoryBot.create_list(:projeto, 5)
    end
    it "deve criar 5 projetos" do
      # Cria 5 projetos usando a fábrica :projeto
      expect(Projeto.count).to eq(5)
    end

    it "exclui o primeiro projeto, restando 4" do
      Projeto.first.delete
      
      # Verifica se existem 5 registros na tabela de projetos
      expect(Projeto.count).to eq(4)
    end
  end
end
