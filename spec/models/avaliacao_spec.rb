require 'rails_helper'

RSpec.describe Avaliacao, type: :model do

  describe "Valida gravacao na tabela" do
    before do
      projeto = FactoryBot.create(:projeto)
      FactoryBot.create(:avaliacao, projeto: projeto)
    end

    it "deve criar 1 avaliacao" do
      # Cria 1 avaliacao usando a fábrica :avaliacao
      expect(Avaliacao.count).to eq(1)
    end

    it "exclui a primeira avaliacao, nao sobrando nenhuma" do
      Avaliacao.first.delete
      
      # Verifica se não existem mais registros
      expect(Avaliacao.count).to eq(0)
    end
  end
end
