require 'rails_helper'

RSpec.describe Avaliacao, type: :model do

  describe "Valida gravacao na tabela" do
    before do
      projeto = FactoryBot.create(:projeto)
      avaliacao = FactoryBot.create(:avaliacao, projeto: projeto)
      criterio = FactoryBot.create(:criterio)
      
      FactoryBot.create(:nota, criterio: criterio, avaliacao: avaliacao)
    end

    it "deve criar 1 nota" do
      # Cria 1 avaliacao usando a fábrica :nota
      expect(Nota.count).to eq(1)
    end

    it "exclui a primeira nota, nao sobrando nenhuma" do
      Nota.first.delete
      
      # Verifica se não existem mais registros
      expect(Nota.count).to eq(0)
    end
  end
end
