require 'rails_helper'

RSpec.describe Criterio, type: :model do

  describe "Valida gravacao na tabela" do
    before do
      FactoryBot.create_list(:criterio, 5)
    end
    it "deve criar 5 criterios" do
      # Cria 5 criterios usando a fábrica :criterio
      expect(Criterio.count).to eq(5)
    end

    it "exclui o primeiro criterio, restando 4" do
        Criterio.first.delete
      
      # Verifica se existem 5 registros na tabela de criterios
      expect(Criterio.count).to eq(4)
    end
  end
end
