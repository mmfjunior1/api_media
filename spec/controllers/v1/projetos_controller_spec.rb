require 'rails_helper'

RSpec.describe V1::ProjetosController, type: :controller do
  describe "POST #create" do
    context "com parâmetros válidos" do
      it "cria um novo projeto" do
        projeto_params = {
          projeto: {
            nome: "Projeto Teste",
            media_total: 3
          }
        }

        expect {
          post :create, params: projeto_params, format: :json
        }.to change(Projeto, :count).by(1)

        expect(response).to have_http_status(:created)
      end
    end
  end

  describe "GET #index" do
    context "lista os projetos" do
      it "retorna uma lista de projetos" do
        projetos = FactoryBot.create_list(:projeto, 3)

        get :index, format: :json

        expect(response).to have_http_status(:success)

        expect(response.body).to eq(projetos.to_json)
      end
    end
  end
end