require 'rails_helper'

RSpec.describe V1::NotasAvaliacaoController, type: :controller do
  describe "POST #create" do
    context "com parâmetros válidos" do
      it "Cria duas notas: 8 e 5. Media ponderada será 6.5" do

        projeto = FactoryBot.create(:projeto)
        criterio = FactoryBot.create(:criterio)
        avaliacao = FactoryBot.create(:avaliacao, projeto: projeto)

        nota_params = {
          notas_avaliacao: {
            criterio_id: criterio.id,
            avaliacao_id: avaliacao.id,
            nota: 8
          }
        }
        expect {
          post :create, params: nota_params, format: :json
        }.to change(Nota, :count).by(1)

        nota_params = {
          notas_avaliacao: {
            criterio_id: criterio.id,
            avaliacao_id: avaliacao.id,
            nota: 5
          }
        }
        expect {
          post :create, params: nota_params, format: :json
        }.to change(Nota, :count).by(1)

        expect(response).to have_http_status(:created)
        expect(Avaliacao.first.media_ponderada).to eq(6.5)
      end
    end
  end
end
