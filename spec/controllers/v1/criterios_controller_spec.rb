require 'rails_helper'

RSpec.describe V1::CriteriosController, type: :controller do
  describe "POST #create" do
    context "com parâmetros válidos" do
      it "cria um novo criterio" do
        criterios_params = {
          criterio: {
            peso: 2
          }
        }

        expect {
          post :create, params: criterios_params, format: :json
        }.to change(Criterio, :count).by(1)

        expect(response).to have_http_status(:created)
      end

      it "edita um criterio cujo peso era 2 e virou 3" do
        criterios = FactoryBot.create(:criterio, peso: 2)
        criterio = Criterio.first

        expect(criterio.peso).to eq(2)

        criterios_params = {
          criterio: {
            peso: 3,
            id: criterio.id
          }
        }

        post :create, params: criterios_params, format: :json
        
        expect(Criterio.first.peso).to eq(3)
      end
    end
  end

  describe "GET #index" do
    context "lista os criterios" do
      it "retorna uma lista de criterios" do
        criterios = FactoryBot.create_list(:criterio, 3)

        get :index, format: :json

        expect(response).to have_http_status(:success)

        expect(response.body).to eq(criterios.to_json)
      end
    end
  end
end