require 'rails_helper'

RSpec.describe V1::AvaliacaoController, type: :controller do
  describe "POST #create" do
    context "com parâmetros válidos" do
      it "cria uma nova avaliacao" do

        projeto = FactoryBot.create(:projeto)

        avaliacao_params = {
          avaliacao: {
            projeto_id: projeto.id
          }
        }
        
        expect {
          post :create, params: avaliacao_params, format: :json
        }.to change(Avaliacao, :count).by(1)
        expect(response).to have_http_status(:created)
      end
    end
  end
end
